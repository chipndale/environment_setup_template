#This is just a shortcut to get you to the 
#Virtuoso environment environment VARIABLE setups are 
#separeted for the environment creations 

# It is assumed that this file is in 
# PROJECT/environment_setup#
# Creates Virtuoso environment under PROJECT

# Variables you need to define
set kit="<NAME_OF_THE_KIT>"
set ver="<VERSION_OF_THE_KIT>"
set extid="_`whoami`"

#Rest of the file should not need to be edited.
set called=($_)
set scriptfp="`readlink -f $called[2]`"
set scriptdir="`dirname $scriptfp`"
set project=`cd $scriptdir/..`

set virtuosodir="${project}/virtuoso_${kit}_${ver}${extid}"

# This creates analog desigg environment with configure AND 
# Sources the sourceme script from the created envirounment.
$scriptdir/configure -p $project -k $kit -v $ver -e ${extid}
echo "Changing to $virtuosodir"
cd $virtuosodir
echo "Sourcing the sourceme.csh"
source sourceme.csh
unset called
unset scriptfp
unset project
unset kit
unset ver
unset extid
unset virtuosodir

