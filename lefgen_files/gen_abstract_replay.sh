#Generate abstract replay
# Layer Mapping starting from common diffusion layer
# Refer as ${LAYERMAP[0]}
# This file is used by generate_lef.sh
#layers from top to bottom, diffusion first
LAYERMAP=( "" "" "" "" "" "" "" "" "" "" "", "")
#Vias from top to bottom
VIAMAP=( "" "" "" "" "" "" "" "" "" "" "")

CURRENTFILE="${TARGETDIR}/abstract.replay"
echo "Generating ${CURRENTFILE}"
cat << EOF > "${CURRENTFILE}"
absSkillMode()
absSetLibrary("${LIBNAME}")
absDeselectAllBins()
absSelectBin("Block")
absSelectCells()
absMoveSelectedCellsToBin("Core")
absDeselectCells()
absDeselectAllBins()
absSelectBin("Core")
absDeselectCells()
absSelectCell("${CELLNAME}")
absMoveSelectedCellsToBin("Block")
absDeselectAllBins()
absSetOption("OptimizeBigDesignMemory" "true")
absSetOption("QuickAbstract" "true")
;absSetOption("suppressMessage" "13005")
absSetOption("suppressMessage" "")
absSetOption("ExtractShapeLimit" "100000")
absSetOption("ReadOnlyTechnology" "true")

absSelectBin("Block")
absSetBinOption("Block" "PinsPowerNames" "${SUPNETS}")
absSetBinOption("Block" "PinsGroundNames" "${GNDNETS}")
absSetBinOption("Block" "PinsGeomSearchLevel" "10")
absSetBinOption("Block" "PinsTextPreserveLabels" "true")
;absSetBinOption("Block" "PinsBoundaryCreate" "as needed")
;absSetBinOption("Block" "PinsBoundaryCreate" "off")
absPins()

absSetBinOption("Core"  "ExtractSig" "false")
absSetBinOption("Block" "ExtractSig" "false")
absSetBinOption("IO"    "ExtractSig" "false")
absSetBinOption("Core"  "ExtractPwr" "false")
absSetBinOption("Block" "ExtractPwr" "false")
absSetBinOption("IO"    "ExtractPwr" "false")

;absSetBinOption("Block" "ExtractPwr" "true")
absSetBinOption("Block" "ExtractPwr" "false")
absSetBinOption("Block" "ExtractNumLevelsPwr" "5")
absSetBinOption("Block" "ExtractPinLayersPwr" "${LAYERMAP[8]} ${LAYERMAP[9]} ${LAYERMAP[11]} ")
absSetBinOption("Block" "ExtractLayersPwr" "${LAYERMAP[7]} ${VIAMAP[7]} ${LAYERMAP[8]} ${VIAMAP[8]} ${LAYERMAP[9]} ${VIAMAP[9]} ${LAYERMAP[11]} ")
absSetBinOption("Block" "ExtractAntennaSizeInput" "true")
absSetBinOption("Block" "ExtractAntennaSizeOutput" "true")
absSetBinOption("Block" "ExtractAntennaSizeInout" "true")
absSetBinOption("Block" "ExtractAntennaMetalArea" "true")
absSetBinOption("Block" "ExtractAntennaGate" "(${LAYERMAP[1]} (${LAYERMAP[1]} and ${LAYERMAP[0]})) ")
absSetBinOption("Block" "ExtractDiffAntennaLayers" "true")
absSetBinOption("Block" "ExtractAntennaLayers" "${LAYERMAP[2]} ${LAYERMAP[3]} ${LAYERMAP[4]} ${LAYERMAP[5]} ${LAYERMAP[6]} ${LAYERMAP[7]} ${LAYERMAP[8]} ${LAYERMAP[9]} ${LAYERMAP[11]} ${VIAMAP[9]} ${VIAMAP[2]} ${VIAMAP[3]} ${VIAMAP[4]} ${VIAMAP[5]} ${VIAMAP[6]} ${VIAMAP[7]} ${VIAMAP[8]} ${VIAMAP[1]} ${LAYERMAP[1]} (${LAYERMAP[0]} (${LAYERMAP[0]} andnot ${LAYERMAP[1]})) ")
absSetBinOption("Block" "ExtractConnectivity" "(${LAYERMAP[2]} ${LAYERMAP[3]} ${VIAMAP[2]})(${LAYERMAP[3]} ${LAYERMAP[4]} ${VIAMAP[3]})(${LAYERMAP[4]} ${LAYERMAP[5]} ${VIAMAP[4]})(${LAYERMAP[5]} ${LAYERMAP[6]} ${VIAMAP[5]})(${LAYERMAP[6]} ${LAYERMAP[7]} ${VIAMAP[6]})(${LAYERMAP[7]} ${LAYERMAP[8]} ${VIAMAP[7]})(${LAYERMAP[8]} ${LAYERMAP[9]} ${VIAMAP[8]})(${LAYERMAP[9]} ${LAYERMAP[11]} ${VIAMAP[9]})(${LAYERMAP[1]} ${LAYERMAP[2]} ${VIAMAP[1]})(${LAYERMAP[0]} ${LAYERMAP[1]} ${VIAMAP[0]})")
absExtract()

absSetBinOption("Block" "AbstractAdjustBoundaryPinsPwr" "false")
;absSetBinOption("Block" "AbstractBlockageCoverLayersDist" "(${LAYERMAP[3]} 10) (${LAYERMAP[4]} 10) (${LAYERMAP[5]} 10) (${LAYERMAP[6]} 10) (${LAYERMAP[7]} 10) (${LAYERMAP[8]} 10) (${LAYERMAP[9]} 10)")
;absSetBinOption("Block" "AbstractBlockageCoverLayers" "${LAYERMAP[2]}")
;absSetBinOption("Block" "AbstractBlockageCoverLayers" "${LAYERMAP[2]} ${LAYERMAP[3]} ${LAYERMAP[4]} ${LAYERMAP[5]}") 
absSetBinOption("Block" "AbstractBlockageCoverLayers" "")
;absSetBinOption("Block" "AbstractBlockageDetailedLayers" "${LAYERMAP[2]} ${LAYERMAP[3]} ${LAYERMAP[4]} ${LAYERMAP[5]} ${LAYERMAP[6]} ${LAYERMAP[7]} ${LAYERMAP[8]} ${LAYERMAP[9]} ${LAYERMAP[11]} ")
absSetBinOption("Block" "AbstractBlockageDetailedLayers" "")
;absSetBinOption("Block" "AbstractBlockageShrinkWrapLayers" "${LAYERMAP[2]} ${LAYERMAP[3]} ${LAYERMAP[4]} ${LAYERMAP[5]} ${LAYERMAP[6]} ${LAYERMAP[7]} ${LAYERMAP[8]} ${LAYERMAP[9]} ${LAYERMAP[11]} ")
absSetBinOption("Block" "AbstractBlockageShrinkWrapLayers" "${LAYERMAP[2]} ${LAYERMAP[3]} ${LAYERMAP[4]} ${LAYERMAP[5]} ${LAYERMAP[6]} ${LAYERMAP[7]} ${LAYERMAP[8]} ${LAYERMAP[9]} ${LAYERMAP[11]} ")
absSetBinOption("Block" "AbstractBlockageShrinkTracks" "(${LAYERMAP[2]} 2) (${LAYERMAP[3]} 2) (${LAYERMAP[4]} 2) (${LAYERMAP[5]} 2) (${LAYERMAP[6]} 2) (${LAYERMAP[7]} 2) (${LAYERMAP[8]} 2) (${LAYERMAP[9]} 2) (${LAYERMAP[11]} 2)")
;absSetBinOption("Block" "AbstractBlockageShrinkAdjust" "(${LAYERMAP[2]} 2) (${LAYERMAP[3]} 2) (${LAYERMAP[4]} 2) (${LAYERMAP[5]} 2) (${LAYERMAP[6]} 2) (${LAYERMAP[7]} 2) (${LAYERMAP[8]} 2) (${LAYERMAP[9]} 2) (${LAYERMAP[11]} 2)")
;absSetBinOption("Block" "AbstractAdjustBoundaryPinsSig" "true")
absSetBinOption("Block" "AbstractAdjustBoundaryPinsSig" "false")
;absSetBinOption("Block" "AbstractAdjustBoundaryPinsSigDist" "50")
;absSetBinOption("Block" "AbstractAdjustRingPinsPwr" "true")
;absSetBinOption("Block" "AbstractAdjustRingPinsDist" "0")
;absSetBinOption("Block" "AbstractAdjustFollowRingPin" "true")

absSetBinOption("Block" "AbstractBlockageFracture" "true")
absSetBinOption("Block" "BlockageLargeShapePct"      "20")
absSetBinOption("Block" "BlockageLargeShape"         "false")
;absSetBinOption("Block" "BlockageLargePurposeList"   "boundary")

; True (default) makse the boundary pisns square.
;absSetBinOption( "Core" "AbstractAdjustBoundaryPinsSig" "false")
;absSetBinOption( "Core" "AbstractAdjustBoundaryPinsPwr" "false")

absAbstract()

absSetOption("ExportLEFVersion" "5.5")
absSetOption("ExportLEFBin" "Block")
absSetOption("ExportLEFFile" "${CELLNAME}.lef")
absSetOption("ExportGeometryLefData" "true")
absSetOption("ExportTechLefData" "false")
absExportLEF()
absSetOption("ExportOptionsFile" "${LIBNAME}/.abstract.options" )
absExportOptions()
absExit()
EOF

